﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpCondition
{
    class Program
    {
        static void Main(string[] args)
        {
            var number = 10;
            if ((int)Stocks.FirstStock == 200)
            {
                Console.WriteLine("inside");
            }

            Stocks newStock;

            if (Enum.TryParse("FirstStock", out newStock)) //bu değer enum içerisinde var mı diye kontrol ediyor
            {
                Console.WriteLine("içeride");
            }

            //single line if kullanımı
            Console.WriteLine(number == 100 ? "its true" : "its false"); // soru işareti if iki nokta else anlamına gelmekte

            var sayi = Console.ReadLine();
            var Ssayi = Convert.ToString(sayi);

            if(Ssayi.Length == 1) Console.WriteLine("Tek basamaklı");
            else if(Ssayi.Length ==2 ) Console.WriteLine("İki basamaklı");
            else if (Ssayi.Length == 3) Console.WriteLine("Üç basamaklı");
            else if (Ssayi.Length == 4) Console.WriteLine("Dört basamaklı");
            else if (Ssayi.Length == 5) Console.WriteLine("Beş basamaklı");

            Console.ReadLine();
        }

        enum Stocks
        {
            FirstStock = 200, SecondStock = 300, ThirdStock = 400
        }
    }
}
